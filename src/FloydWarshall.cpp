#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <utility>

using namespace std;

vector<string> user_name;

vector<vector<int>> floyd_warshall(vector<vector<int>> matrix){
  for(int k = 0; k < matrix.size(); k++)
    for(int i = 0; i < matrix.size(); i++)
      for(int j = 0; j < matrix.size(); j++)
        matrix[i][j] = matrix[i][j] || (matrix[i][k] && matrix[k][j]);
  return matrix;
}

vector<vector<int>> process_input(string input_file_name){
  set<string> user_names_set;
  map<string,int> user_name_to_index;
  vector<pair<int,int>> relation;
  string origin_user, destination_user, scape;

  ifstream input_file(input_file_name);

  while(input_file >> origin_user >> scape >> destination_user){
    if(user_names_set.find(origin_user) == user_names_set.end()){
      user_name_to_index.insert(make_pair(origin_user, user_name.size()));
      user_name.push_back(origin_user);
      user_names_set.insert(origin_user);
    }
    if(user_names_set.find(destination_user) == user_names_set.end()){
      user_name_to_index.insert(make_pair(destination_user, user_name.size()));
      user_name.push_back(destination_user);
      user_names_set.insert(destination_user);
    }
    relation.push_back(make_pair(user_name_to_index[origin_user], user_name_to_index[destination_user]));
  }

  input_file.close();

  vector<int> tmpvec (user_name.size(), 0);
  vector<vector<int>> adjacency_matrix (user_name.size(), tmpvec);

  for(int i = 0; i < relation.size(); i++)
    adjacency_matrix[relation[i].first][relation[i].second] = 1;

  return adjacency_matrix;
}

void produce_output(vector<vector<int>> matrix){
  vector<string> community;
  ofstream output_file("salida_p3_luicent_iredeol.txt");

  for(int i = 0; i < matrix.size(); i++){
    if(matrix[i][i] == 1){
      community.clear();
      community.push_back(user_name[i]);
      for(int j = i+1; j < matrix.size(); j++)
        if(matrix[i][j] == 1 && matrix[j][i] == 1){
          community.push_back(user_name[j]);
          matrix[j][j] = 0;
        }

      if(community.size() >= 3){
        output_file << "COMUNIDAD:\n" << community[0];
        for(int k = 1; k < community.size(); k++)
          output_file << ", " << community[k];
        output_file << '\n';
      }
    }
  }

  output_file.close();
}

int main(){
  string input_file_name;

  cout << "Escriba el nombre del fichero de entrada: ";
  cin >> input_file_name;

  vector<vector<int>> adjacency_matrix = process_input(input_file_name);
  vector<vector<int>> transformed_matrix = floyd_warshall(adjacency_matrix);
  produce_output(transformed_matrix);

  cout << "Terminado\n";

  return 0;
}
