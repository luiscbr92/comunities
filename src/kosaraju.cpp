#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <utility>
#include <algorithm>

using namespace std;

vector<string> user_name;
vector<int> order;
vector<bool> used;
vector<int> component;
vector<vector<int>> communities;
vector<vector<int>> adjacency_list;
vector<vector<int>> inverse_adjacency_list;

void dfs(int vertex, bool in_reverse_graph){
  if(in_reverse_graph == false){
    used[vertex] = true;
    for(int i = 0; i < adjacency_list[vertex].size(); i++)
      if(!used[adjacency_list[vertex][i]])
        dfs(adjacency_list[vertex][i], false);
    order.insert(order.begin(), vertex);
  }
  else{
    used[vertex] = true;
    component.push_back(vertex);
    for(int i = 0; i < inverse_adjacency_list[vertex].size(); i++)
      if(!used[inverse_adjacency_list[vertex][i]])
        dfs(inverse_adjacency_list[vertex][i], true);
  }
}

void kosaraju(){
  fill(used.begin(), used.end(), false);
  for(int i = 0; i < adjacency_list.size(); i++)
    if(!used[i])
      dfs(i, false);

  fill(used.begin(), used.end(), false);
  for(int i = 0; i < order.size(); i++)
    if(!used[order[i]]){
      component.clear();
      dfs(order[i], true);
      communities.push_back(component);
    }
}

void process_input(string input_file_name){
  set<string> user_names_set;
  map<string,int> user_name_to_index;
  string origin_user, destination_user, scape;
  vector<int> empty_vector;

  ifstream input_file(input_file_name);

  while(input_file >> origin_user >> scape >> destination_user){
    if(user_names_set.find(origin_user) == user_names_set.end()){
      user_name_to_index.insert(make_pair(origin_user, user_name.size()));
      user_name.push_back(origin_user);
      user_names_set.insert(origin_user);
      adjacency_list.push_back(empty_vector);
      inverse_adjacency_list.push_back(empty_vector);
    }
    if(user_names_set.find(destination_user) == user_names_set.end()){
      user_name_to_index.insert(make_pair(destination_user, user_name.size()));
      user_name.push_back(destination_user);
      user_names_set.insert(destination_user);
      adjacency_list.push_back(empty_vector);
      inverse_adjacency_list.push_back(empty_vector);
    }
    adjacency_list[user_name_to_index[origin_user]].push_back(user_name_to_index[destination_user]);
    inverse_adjacency_list[user_name_to_index[destination_user]].push_back(user_name_to_index[origin_user]);
  }

  input_file.close();

  used.resize(user_name.size());

}

void produce_output(vector<vector<int>> communities){
  ofstream output_file("salida_p3_luicent_iredeol.txt");

  for(int i = 0; i < communities.size(); i++){
    if(communities[i].size() >= 3){
      output_file << "COMUNIDAD:\n" << user_name[communities[i][0]];
      for(int j = 1; j < communities[i].size(); j++)
        output_file << ", " << user_name[communities[i][j]];
      output_file << '\n';
    }
  }

  output_file.close();
}

int main(){
  string input_file_name;

  cout << "Escriba el nombre del fichero de entrada: ";
  cin >> input_file_name;

  process_input(input_file_name);

  kosaraju();

  produce_output(communities);

  cout << "Terminado\n";

  return 0;
}
